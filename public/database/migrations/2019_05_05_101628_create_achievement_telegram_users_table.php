<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAchievementTelegramUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('achievement_telegram_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('telegram_user_id');
            $table->unsignedBigInteger('achievement_id');

            $table->foreign('telegram_user_id')
                ->references('id')->on('telegram_users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('achievement_id')
                ->references('id')->on('achievements')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('achievement_telegram_users');
    }
}
