<?php

use App\Order;
use App\TelegramUser;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTelegramUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_telegram_user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('telegram_user_id');
            $table->unsignedBigInteger('order_id');
            $table->enum('pay_status', Order::PAY_STATUSES)->default(Order::PAY_STATUS_NOT);
            $table->enum('user_status', TelegramUser::USER_ORDER_STATUSES)->default(TelegramUser::USER_ORDER_STATUS_USER);

            $table->foreign('order_id')
                ->references('id')->on('orders')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('telegram_user_id')
                ->references('id')->on('telegram_users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_telegram_user');
    }
}
