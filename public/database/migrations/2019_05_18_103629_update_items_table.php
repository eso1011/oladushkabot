<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->dropForeign('items_order_telegram_user_id_foreign');
            $table->dropColumn('order_telegram_user_id');
            $table->unsignedBigInteger('order_id');
            $table->integer('telegram_user_id');

            $table->foreign('order_id')
                ->references('id')->on('orders')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('telegram_user_id')
                ->references('id')->on('telegram_users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->unsignedBigInteger('order_telegram_user_id');
            $table->foreign('order_telegram_user_id')
                ->references('id')->on('order_telegram_user')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->dropForeign('items_order_id_foreign');
            $table->dropColumn('order_id');

            $table->dropForeign('items_order_telegram_user_id_foreign');
            $table->dropColumn('order_telegram_user_id');
        });
    }
}
