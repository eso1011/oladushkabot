<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Achievements extends Model
{
    protected $guarded = ['id'];

    const OCCUPATION_STATUSES = [
        self::OCCUPATIONS_STATUS_ASSIGNED,
        self::OCCUPATIONS_STATUS_NOT_ASSIGNED,
        self::OCCUPATIONS_STATUS_PUBLIC,
    ];

    const OCCUPATIONS_STATUS_ASSIGNED = 'Assigned';
    const OCCUPATIONS_STATUS_NOT_ASSIGNED= 'Not assigned';
    const OCCUPATIONS_STATUS_PUBLIC= 'Public';
}
