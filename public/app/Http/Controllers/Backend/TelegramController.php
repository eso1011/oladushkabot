<?php

namespace App\Http\Controllers\Backend;

use App\Item;
use App\Order;
use App\OrderTelegramUser;
use App\Place;
use App\Services\OrderService;
use App\TelegramUser;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Exceptions\TelegramResponseException;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;

class TelegramController extends Controller
{
    /**
     *
     */
    public function webhook()
    {
//        Log::info(Telegram::getWebhookUpdates());
        $botUsername = env('BOT_USERNAME');
        $updates = Telegram::getWebhookUpdates();
        if (isset($updates['message'])) {
            $message = $updates['message'];
            /** @var TelegramUser $telegramUser */
            $telegramUser = TelegramUser::find($message['from']['id']);
            if (!$telegramUser) {
                /** @var TelegramUser $telegramUser */
                $telegramUser = TelegramUser::create($message['from']);
            }
            if ($telegramUser && array_diff_assoc($telegramUser->toArray(), $message['from']) <= 0) {
                $telegramUser->update($message['from']);
            }
            if (isset($message['entities']) && ($message['entities'][0]['type'] === 'bot_command')) {
                $telegramUser->current_action = null;
                $telegramUser->save();
                try {
                    Telegram::commandsHandler(true);
                } catch (TelegramResponseException $e) {
                    if (($message['chat']['type'] === "group") && ($e->getCode() === 403)) {
                        Telegram::sendMessage([
                            'chat_id' => $message['chat']['id'],
                            'text' => "Упс. Бот не может отправить вам личное сообщение. Убедитесь, что активировали бота {$botUsername} с помощью команды /start в личных сообщениях.",
                        ]);
                    } else {
                        Log::error('NOT 403');
                        Log::error($e->getMessage());
                    }
                }
            } else {
                //...simple text
                $action = json_decode($telegramUser->current_action, true);
                if (isset($action['action'])) {
                    switch ($action['action']) {
                        case 'new_place_set_name':
                            if (strlen($message['text']) < 3) {
                                $telegramUser->sendMessage("Имя должно содержать как минимум 3 символа. Введите другое");
                                return null;
                            }
                            $action['data']['name'] = $message['text'];
                            $telegramUser->setCurrentActionData('new_place_set_url', $action['data']);
                            $telegramUser->sendMessage("Введите URL.");
                            break;
                        case 'new_place_set_url':
                            if (!filter_var($message['text'], FILTER_VALIDATE_URL)) {
                                $telegramUser->sendMessage("URL должен быть корректным адресом. Введите другой.");
                                return null;
                            }
                            if (strlen($message['text']) > 255) {
                                $telegramUser->sendMessage("URL имеет слишком много символов.");
                                return null;
                            }
                            if (Place::where('url', $message['text'])->first()) {
                                $telegramUser->sendMessage("Место с таким URL уже существует. Введите другой.");
                                return null;
                            }
                            $action['data']['url'] = $message['text'];
                            /** @var Place $place */
                            $place = Place::create($action['data']);
                            $telegramUser->setCurrentAction([]);
                            $telegramUser->sendMessage("Место успешно создано!\n{$place->id}. {$place->name} - {$place->url}");
                            break;
                        case 'new_order_choose_place':
                            if (!is_numeric($message['text'])) {
                                $telegramUser->sendMessage("ID должен быть числом. Введите другой.");
                                return null;
                            }
                            if (!Place::find($message['text'])) {
                                $telegramUser->sendMessage("Места с таким ID не существует. Введите другой.");
                                return null;
                            }
//                            create and attach place
                            /** @var Order $order */
                            $order = Order::create(['place_id' => $message['text']]);
                            $order->telegramUsers()->attach($telegramUser->id, ['user_status' => TelegramUser::USER_ORDER_STATUS_ADMIN, 'pay_status' => Order::PAY_STATUS_PAID]);
//                            reset current action
                            $telegramUser->setCurrentAction([]);
                            $username = $order->telegramUsers->first()->username ?? "CHAT_ID={$telegramUser->id}";
                            $username = $username ? $username : "CHAT_ID={$telegramUser->id}";
                            //sending final message
                            $telegramUser->sendMessage("Заказ успешно создан.\n{$order->id}. {$order->place->name} - {$order->place->url}\nСоздатель: {$username}\nПосле подтверждения заказа не забудьте сменить статус заказа на Подтвержден (/order_change_status). Это заблокирует заказ для присоединения новых пользователей и оповестит сопартийцев о том, что заказ был подтвержден.");
                            break;
                        case 'join_order_choose_order':
                            if (!is_numeric($message['text'])) {
                                $telegramUser->sendMessage("ID должен быть числом. Введите другой.");
                                return null;
                            }
                            /** @var Order $order */
                            $order = Order::where(['id' => $message['text'], 'status' => Order::STATUS_ACTIVE])->doesntHaveTelegramUser($telegramUser)->first();
                            if (!$order) {
                                $telegramUser->sendMessage("Активного заказа с таким ID не существует или вы уже принимаете участиве в нем. Введите другой.");
                                return null;
                            }
                            $order->telegramUsers()->attach($telegramUser->id, [
                                'user_status' => TelegramUser::USER_ORDER_STATUS_USER,
                                'pay_status' => Order::PAY_STATUS_NOT
                            ]);
                            $telegramUser->setCurrentAction([]);
                            $telegramUser->sendMessage("Вы добавлены в заказ {$order->getOrderName()}.");
                            $adminUsers = $order->getAdmins();
                            /** @var TelegramUser $adminUser */
                            foreach ($adminUsers as $adminUser) {
                                $adminUser->sendMessage("Пользователь @{$telegramUser->username} присоединился к заказу {$order->getOrderName()}.");
                            }
                            break;
                        case 'confirm_order_choose_order':
                            if (!is_numeric($message['text'])) {
                                $telegramUser->sendMessage("ID должен быть числом. Введите другой.");
                                return null;
                            }
                            /** @var Order $order */
                            $order = Order::statusAndHasAdmin(Order::STATUS_ACTIVE, $telegramUser)->where('id', $message['text'])->first();
                            if (!$order) {
                                $telegramUser->sendMessage("Активного заказа с таким ID не существует или вы не являетесь админом. Введите другой.");
                                return null;
                            }
                            $telegramUser->setCurrentAction([]);
                            $order->update(['status' => Order::STATUS_CONFIRMED]);
                            OrderService::sendMessagesToRecipients($order, $telegramUser, "Ваш заказ {$order->getOrderName()} был переведен в статус Подтвержден. Вы получите уведомление как только заказ будет доставлен 🚚🚚🚚");
                            break;
                        case 'deliver_order_choose_order':
                            if (!is_numeric($message['text'])) {
                                $telegramUser->sendMessage("ID должен быть числом. Введите другой.");
                                return null;
                            }
                            /** @var Order $order */
                            $order = Order::statusAndHasAdmin(Order::STATUS_CONFIRMED, $telegramUser)->where('id', $message['text'])->first();
                            if (!$order) {
                                $telegramUser->sendMessage("Подтвержденного заказа с таким ID не существует или вы не являетесь админом. Введите другой.");
                                return null;
                            }
                            $telegramUser->setCurrentAction([]);
                            $order->update(['status' => Order::STATUS_DELIVERED]);

                            /** @var TelegramUser $recipientUser */
                            $telegramUser->sendMessage("Статус вашего заказа {$order->getOrderName()} изменен на {$order->status}");
                            $order->sendMessagesToRecipients($telegramUser, 'Ваш заказ прибыл в точку сброса. Bon appetit! 🍏🍗🍙🍖🍜');
                            break;
                        case 'change_order_status':
                            if (!is_numeric($message['text'])) {
                                $telegramUser->sendMessage("ID должен быть числом. Введите другой.");
                                return null;
                            }
                            /** @var Order $order */
                            $order = Order::hasAdmin($telegramUser)->notFinished()->where('id', $message['text'])->first();
                            if (!$order) {
                                $telegramUser->sendMessage("Такого заказа не существует, или вы не являетесь админом.");
                                return null;
                            }
                            $telegramUser->setCurrentActionData('change_order_set_status', ['order_id' => $order->id, 'status' => $order->status]);
                            $keyboard = [];
                            switch ($order->status) {
                                case Order::STATUS_ACTIVE:
                                    $keyboard[] = [
                                        ['text' => Order::STATUS_CONFIRMED],
                                        ['text' => Order::STATUS_DELIVERED],
                                        ['text' => Order::STATUS_DECLINED],
                                    ];
                                    break;
                                case Order::STATUS_CONFIRMED:
                                    $keyboard[] = [
                                        ['text' => Order::STATUS_DELIVERED],
                                        ['text' => Order::STATUS_DECLINED],
                                    ];
                                    break;
                            }
                            $replyMarkup = Keyboard::make([
                                'keyboard' => $keyboard,
                                'resize_keyboard' => true,
                                'one_time_keyboard' => true
                            ]);
                            $telegramUser->sendMessage("Выберите статус заказа {$order->getOrderName()}:", $replyMarkup);
                            break;
                        case 'change_order_set_status':
                            /** @var Order $order */
                            if (!in_array($message['text'], Order::STATUSES)) {
                                $telegramUser->sendMessage("Такого статуса не существует. Введите другой.");
                                return null;
                            }
                            $order = Order::hasAdmin($telegramUser)->notFinished()->where('id', $action['data']['order_id'])->first();
                            if (!$order) {
                                $telegramUser->sendMessage("Такого заказа не существует, или вы не являетесь админом.");
                                return null;
                            }
                            switch ($order->status) {
                                case Order::STATUS_ACTIVE:
                                    if (!in_array($message['text'], Order::ALLOWED_FROM_ACTIVE_STATUSES)) {
                                        $telegramUser->sendMessage('Такой статус нельзя присвоить данному заказу. Выберите другой.');
                                        return null;
                                    }
                                    break;
                                case Order::STATUS_CONFIRMED:
                                    if (!in_array($message['text'], Order::ALLOWED_FROM_CONFIRMED_STATUSES)) {
                                        $telegramUser->sendMessage('Такой статус нельзя присвоить данному заказу. Выберите другой.');
                                        return null;
                                    }
                                    break;
                            }
                            switch ($message['text']) {
                                case Order::STATUS_CONFIRMED:
                                    $order->sendMessagesToRecipients($telegramUser, "Ваш заказ {$order->getOrderName()} был переведен в статус Подтвержден. Вы получите уведомление как только заказ будет доставлен 🚚🚚🚚", true);
                                    $telegramUser->sendMessage("Не забудьте перевести заказ в статус Доставлен, чтобы вашим сопартийцам пришло сообщение о доставке (/order_change_status).");
                                    break;
                                case Order::STATUS_DELIVERED:
                                    $allPaid = true;
                                    foreach ($order->telegramUsers as $user) {
                                        if ($user->pivot->pay_status === Order::PAY_STATUS_NOT) {
                                            $allPaid = false;
                                        }
                                    }
                                    if ($allPaid) {
                                        $message['text'] = Order::STATUS_CLOSED;
                                        $order->sendMessageToAdmins("Все пользователи оплатили свои позиции и он был переведен в статус Закрыт.");
                                    } else {
                                        $text = $order->printListOfDefaulterUsers();
                                        $order->sendMessageToAdmins($text);
                                    }
                                    $order->sendMessagesToRecipients($telegramUser, "Ваш заказ {$order->getOrderName()} прибыл в точку сброса. Bon appetit! 🍏🍗🍙🍖🍜", true);
                                    break;
                                case Order::STATUS_DECLINED:
                                    $order->sendMessagesToRecipients($telegramUser, "Заказ {$order->getOrderName()} был отменен админом! 🙅🏿‍♂️🙅🏿‍♂️🙅🏿‍♂");
                                    break;
                            }
                            $order->update(['status' => $message['text']]);
                            $telegramUser->setCurrentAction([]);
                            break;
                        case 'leave_order_choose_order':
                            if (!is_numeric($message['text'])) {
                                $telegramUser->sendMessage("ID должен быть числом. Введите другой.");
                                return null;
                            }
                            /** @var Order $order */
                            $order = Order::where(['id' => $message['text'], 'status' => Order::STATUS_ACTIVE])
                                ->hasTelegramUser($telegramUser, [TelegramUser::USER_ORDER_STATUS_USER])
                                ->first();
                            if (!$order) {
                                $telegramUser->sendMessage("Активного заказа с таким ID не существует или вы не принимаете в нем участия. Введите другой.");
                                return null;
                            }
                            $order->telegramUsers()->detach($telegramUser->id);
                            $order->telegramUsersItems()->detach($telegramUser->id);
//                            $order->items()->detach($telegramUser->id);
                            $telegramUser->setCurrentAction([]);
                            $telegramUser->sendMessage("Вы покинули заказ {$order->getOrderName()}");
                            /** @var Collection $adminUsers */
                            $adminUsers = $order->getAdmins();
                            /** @var TelegramUser $adminUser */
                            foreach ($adminUsers as $adminUser) {
                                $adminUser->sendMessage("Пользователь @{$telegramUser->username} покинул заказ {$order->getOrderName()}.");
                            }
                            break;
                        case 'new_item_select_order':
                            if (!is_numeric($message['text'])) {
                                $telegramUser->sendMessage("ID должен быть числом. Введите другой.");
                                return null;
                            }
                            /** @var Order $order */
                            $order = Order::where(['id' => $message['text'], 'status' => Order::STATUS_ACTIVE])
                                ->hasTelegramUser($telegramUser)
                                ->first();
                            if (!$order) {
                                $telegramUser->sendMessage("Активного заказа с таким ID не существует. Введите другой.");
                                return null;
                            }
                            $telegramUser->sendMessage("Введите имя позиции.");
                            $telegramUser->setCurrentActionData('new_item_name_insert', ['order_id' => $order->id]);
                            break;
                        case 'new_item_name_insert':
                            if (strlen($message['text']) < 3) {
                                $telegramUser->sendMessage("Имя должно содержать как минимум 3 символа. Введите другое");
                                return null;
                            }
                            $action['data']['name'] = $message['text'];
                            $telegramUser->sendMessage("Введите количество позиций числом (минимум 1).");

                            $telegramUser->setCurrentActionData('new_item_count_insert', $action['data']);
                            break;
                        case 'new_item_count_insert':
                            if (intval($message['text']) <= 0) {
                                $telegramUser->sendMessage("Количество позиций должно быть положительным числом больше 0. Введите другое.");
                                return null;
                            }
                            $action['data']['count'] = intval($message['text']);
                            $telegramUser->sendMessage("Введите стоимость одной позиции.");
                            $telegramUser->setCurrentActionData('new_item_price_insert', $action['data']);
                            break;
                        case 'new_item_price_insert':
                            if (!is_numeric($message['text'])) {
                                $telegramUser->sendMessage("Цена должна быть числом");
                                return null;
                            }
                            $action['data']['price'] = $message['text'];
                            $telegramUser->sendMessage("Введите URL позиции на сайт доставки, если это необходимо. Если URL не нужен, то введите прочерк ('-')");
                            $telegramUser->setCurrentActionData('new_item_url_insert', $action['data']);
                            break;
                        case 'new_item_url_insert':
                            if (!filter_var($message['text'], FILTER_VALIDATE_URL) && $message['text'] !== '-') {
                                $telegramUser->sendMessage("URL должен быть валидным адрессом или равняться '-'");
                                return null;
                            }
                            if (strlen($message['text']) > 255) {
                                $telegramUser->sendMessage("URL имеет слишком много символов.");
                                return null;
                            }
                            if ($message['text'] !== '-') {
                                $action['data']['url'] = $message['text'];
                            }
                            /** @var Order $order */
                            $order = Order::find($action['data']['order_id']);
                            if (!$order) {
                                $telegramUser->sendMessage("Такого заказа не существует");
                                return null;
                            }
                            $action['data']['telegram_user_id'] = $telegramUser->id;
                            /** @var Item $item */
                            $item = Item::create($action['data']);
                            $telegramUser->sendMessage("Позиция успешно создана.\n{$item->getItemInfo()}");
                            $telegramUser->setCurrentAction([]);

                            $adminUsers = $order->getAdmins();
                            /** @var TelegramUser $adminUser */
                            foreach ($adminUsers as $adminUser) {
                                if ($adminUser->id !== $telegramUser->id) {
                                    $adminUser->sendMessage("Пользователь @{$telegramUser->username} добавил позицию в заказ {$order->getOrderName()}.\n{$item->getItemInfo()}");
                                }
                            }
                            break;
                        case  'items_list_select_order':
                            if (intval($message['text']) <= 0) {
                                $telegramUser->sendMessage("ID заказа должен быть числом > 0.");
                                return null;
                            }
                            /** @var Order $order */
                            $order = Order::where('id', $message['text'])
                                ->notClosed()
                                ->hasTelegramUser($telegramUser)
                                ->first();
                            if (!$order) {
                                $telegramUser->sendMessage("Такого заказа не существует.");
                                return null;
                            }
                            $text = $order->printListOfItems();
                            $telegramUser->setCurrentAction([]);
                            $telegramUser->sendMessage($text);
                            break;
                        case 'remove_item_select_order':
                            if (!is_numeric($message['text'])) {
                                $telegramUser->sendMessage("ID должен быть числом. Введите другой.");
                                return null;
                            }
                            /** @var Order $order */
                            $order = Order::where(['id' => $message['text'], 'status' => Order::STATUS_ACTIVE])
                                ->hasTelegramUser($telegramUser)
                                ->first();
                            if (!$order) {
                                $telegramUser->sendMessage("Активного заказа с таким ID не существует. Введите другой.");
                                return null;
                            }
                            $text = $order->printListOfUserItems($telegramUser);
                            if (!$text) {
                                $telegramUser->sendMessage("Этот заказ не имеет ваших позиций");
                                $telegramUser->setCurrentAction([]);
                                return null;
                            }
                            $telegramUser->sendMessage($text);
                            $items = $order->getUserListOfItems($telegramUser);
                            $keyboard = [];
                            foreach ($items as $item) {
                                $keyboard[] = [
                                    ['text' => $item->id],
                                ];
                            }
                            $replyMarkup = Keyboard::make([
                                'keyboard' => $keyboard,
                                'resize_keyboard' => true,
                                'one_time_keyboard' => true
                            ]);
                            $telegramUser->sendMessage('Выберите ID позиции.', $replyMarkup);
                            $telegramUser->setCurrentActionData('remove_item_select_item', ['order_id' => $order->id]);
                            break;
                        case 'remove_item_select_item':
                            if (intval($message['text']) <= 0) {
                                $telegramUser->sendMessage("ID должен быть числом. Введите другой.");
                                return null;
                            }
                            /** @var Item $item */
                            $item = Item::where([
                                'order_id' => $action['data']['order_id'],
                                'telegram_user_id' => $telegramUser->id,
                                'id' => $message['text'],
                            ])->first();
                            /** @var Order $order */
                            $order = Order::where(['id' => $action['data']['order_id'], 'status' => Order::STATUS_ACTIVE])
                                ->hasTelegramUser($telegramUser)
                                ->first();
                            if (!$order) {
                                $telegramUser->sendMessage("Активного заказа с таким ID больше не существует. Возможно создатель уже сменил статус");
                                $telegramUser->setCurrentAction([]);
                                return null;
                            }
                            if (!$item) {
                                $telegramUser->sendMessage("Позиции с таким ID не существует в указанном заказе или она не принадлежит вам.");
                                return null;
                            }

                            /** @var TelegramUser $adminUser */
                            $info = $item->getItemInfo();

                            try {
                                $item->delete();
                            } catch (\Exception $e) {
                                Log::error($e->getMessage());
                                Log::error("{$e->getFile()}({$e->getLine()})");
                                $telegramUser->setCurrentAction([]);
                                $telegramUser->sendMessage("Exception was catch! Delete is failed! {$e->getFile()}({$e->getLine()})");
                                return null;
                            }
                            $telegramUser->setCurrentAction([]);
                            $telegramUser->sendMessage("Позиция {$info}. Успешно удалена.");
                            $adminUsers = $order->getAdmins();
                            foreach ($adminUsers as $adminUser) {
                                if ($adminUser->id !== $telegramUser->id) {
                                    $adminUser->sendMessage("Пользователь @{$telegramUser->username} удалил позицию из заказа {$order->getOrderName()}.\n{$info}");
                                }
                            }
                            break;
                        case 'pay_select_order':
                            if (intval($message['text']) <= 0) {
                                $telegramUser->sendMessage("ID должен быть числом. Введите другой.");
                                return null;
                            }
                            /** @var Order $order */
                            $order = Order::where('id', $message['text'])
                                ->notClosed()
                                ->hasTelegramUser($telegramUser)
                                ->hasNotPaidUser($telegramUser)
                                ->first();
                            if (!$order) {
                                $telegramUser->sendMessage("Такого заказа не существует.");
                                return null;
                            }

                            $text = $order->printListOfUserItems($telegramUser);
                            if (!$text) {
                                $telegramUser->setCurrentAction([]);
                                $telegramUser->sendMessage("У вас нет ни одной позиции в данном заказе.");
                                return null;
                            }
                            $telegramUser->sendMessage($text);
                            $telegramUser->setCurrentActionData('pay_select_order', ['order_id' => $order->id]);
                            $replyMarkup = Keyboard::make()->inline()->row(
                                Keyboard::inlineButton(['text' => 'Нет', 'callback_data' => 'decline_pay']),
                                Keyboard::inlineButton(['text' => 'Да', 'callback_data' => 'confirm_pay'])
                            );
                            $telegramUser->sendMessage('Вы уверены, что хотите подтвердить перевод средств?', $replyMarkup);

                            break;
                        case 'update_requisites':
                            if (strlen($message['text']) < 11) {
                                $telegramUser->sendMessage("Реквизиты должны содержать как минимум 11 символов. Введите другое");
                                return null;
                            }
                            if (strlen($message['text']) > 255) {
                                $telegramUser->sendMessage("Слишком длинны реквизиты. Ограничтесь 255 символами.");
                                return null;
                            }
                            $telegramUser->update(['requisites' => $message['text']]);
                            $telegramUser->setCurrentAction([]);
                            $telegramUser->sendMessage('Реквизиты успешно добавлены');
                            break;
                        default:
                            $telegramUser->sendMessage("Ошибка состояния пользователя: {$action['action']}. Обработчик отсутствует.");
                            break;
                    }
                } else {
                    $commands = Telegram::getCommands();

                    $text = '';
                    foreach ($commands as $name => $handler) {
                        $text .= sprintf('/%s - %s' . PHP_EOL, $name, $handler->getDescription());
                    }

                    $telegramUser->sendMessage($text);
                }

            }

        } else if (isset($updates['callback_query'])) {
            $callback = $updates['callback_query'];
            /** @var TelegramUser $telegramUser */
            $telegramUser = TelegramUser::find($callback['from']['id']);
            $action = json_decode($telegramUser->current_action, true);
            if (isset($callback['data']) && !empty($action)) {
                switch ($callback['data']) {
                    case 'decline_pay':
                        $telegramUser->setCurrentAction([]);
                        $telegramUser->sendMessage('Подтверждение перевода отменено.');
                        break;
                    case 'confirm_pay':
                        /** @var Order $order */
                        $order = Order::where('id', $action['data']['order_id'])
                            ->notClosed()
                            ->hasTelegramUser($telegramUser)
                            ->hasNotPaidUser($telegramUser)
                            ->first();
                        if (!$order) {
                            $telegramUser->sendMessage("Такого заказа не существует.");
                            return null;
                        }
                        DB::transaction(function () use ($order, $telegramUser) {
                            $order->telegramUsers()->updateExistingPivot($telegramUser->id, ['pay_status' => Order::PAY_STATUS_PAID]);
                            /** @var TelegramUser $user */
                            $allPaid = true;
                            foreach ($order->telegramUsers as $user) {
                                if ($user->pivot->pay_status === Order::PAY_STATUS_NOT && $user->id !== $telegramUser->id) {
                                    $allPaid = false;
                                }
                            }
                            if ($allPaid and $order->status === Order::STATUS_DELIVERED) {
                                $order->update(['status' => Order::STATUS_CLOSED]);
                                $order->sendMessageToAdmins("Пользователь @{$telegramUser->username} сообщил об оплате. Он был последним и заказ теперь переведен в статус Закрыт.");
                            } else {
                                $order->awareAdmins($telegramUser, 'уведомил об оплате заказа');
                            }
                            $telegramUser->sendMessage('Да не опустеет рука дающего. Создатель заказа оповещен о вашем деянии.');
                            $telegramUser->setCurrentAction([]);
                            });

                        break;
                    default:
                        Telegram::sendMessage([
                            'chat_id' => $callback['from']['id'],
                            'text' => "Ошибка коллбека: {$callback['data']}. Обработчик отсутствует.",
                        ]);
                        break;
                }
            }
        }

    }
}
