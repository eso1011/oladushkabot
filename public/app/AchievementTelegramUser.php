<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AchievementTelegramUser extends Model
{
    protected $guarded = ['id'];
}
