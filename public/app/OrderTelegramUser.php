<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * @property integer id
 * @property string pay_status
 */
class OrderTelegramUser extends Model
{
    protected $guarded = [];
    protected $table='order_telegram_user';

}
