<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Laravel\Facades\Telegram;

/**
 * @property string current_action
 * @property integer id
 * @property string username
 * @property OrderTelegramUser pivot
 * @property string requisites
 */
class TelegramUser extends Model
{
    protected $guarded = [];

    const USER_ORDER_STATUSES = [
        self::USER_ORDER_STATUS_ADMIN,
        self::USER_ORDER_STATUS_USER,
    ];
    const USER_ORDER_STATUS_ADMIN = 'Admin';
    const USER_ORDER_STATUS_USER = 'User';

    /**
     * @param $action
     */
    public function setCurrentAction(array $action)
    {
        if (!empty($action)) {
            $this->current_action = json_encode($action);
        } else {
            $this->current_action = null;
        }
        $this->save();
    }


    /**
     * @param string $type
     * @param array $data
     */
    public function setCurrentActionData(string $type, array $data)
    {
        $action = [
            'action' => $type,
            'data' => $data,
        ];
        $this->current_action = json_encode($action);
        $this->save();
    }

    public function orders(){
        return $this->belongsToMany(Order::class);
    }

    public function sendMessage(string $text, $reply_markup = null){
        try {
            return Telegram::sendMessage([
                'chat_id' => $this->id,
                'text' => $text,
                'reply_markup' => $reply_markup,
            ]);
        }catch (\Exception $exception){
            try{
                $me = TelegramUser::find(135214574);
                Telegram::sendMessage([
                    'chat_id' => $me->id,
                    'text' => "️Ошибка отправки сообщения пользователю @{$this->username}: {$exception->getMessage()} {$exception->getFile()}({$exception->getLine()})⛔️⛔️⛔️",
                    'reply_markup' => $reply_markup,
                ]);
            }catch (\Exception $e){
                Log::error('SEND ME MESSAGE FAILED');
                Log::error($exception->getMessage());
            }
            /** @var TelegramUser $me */

            Log::error('SEND USER FAILED');
            Log::error($exception->getMessage());
        }


    }

    public function items(){
        return $this->hasMany(Item::class);
    }

    public function getItemsByOrder(Order $order){
        return $this->items()->where('order_id', $order->id)->get();
    }
}
