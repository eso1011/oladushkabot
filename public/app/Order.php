<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer id
 * @property Place place
 * @property Collection telegramUsers
 * @property string status
 * @property Collection items
 * @method static Builder statusAndHasAdmin(string $status, TelegramUser $telegramUser)
 * @method static Builder hasAdmin(TelegramUser $telegramUser)
 * @method static Builder notFinished()
 * @see Order::scopeHasAdmin()
 * @see Order::notFinished()
 */
class Order extends Model
{
    protected $guarded = ['id'];

    const STATUSES = [
        self::STATUS_ACTIVE,
        self::STATUS_CONFIRMED,
        self::STATUS_DELIVERED,
        self::STATUS_DECLINED,
        self::STATUS_CLOSED,
    ];

    const FINISHED_STATUSES = [
        self::STATUS_DELIVERED,
        self::STATUS_DECLINED,
        self::STATUS_CLOSED,
    ];

    const ALLOWED_FROM_ACTIVE_STATUSES = [
        self::STATUS_CONFIRMED,
        self::STATUS_DELIVERED,
        self::STATUS_DECLINED,
    ];

    const ALLOWED_FROM_CONFIRMED_STATUSES = [
        self::STATUS_DELIVERED,
        self::STATUS_DECLINED,
    ];

    const STATUS_ACTIVE = 'Active';
    const STATUS_CONFIRMED = 'Confirmed';
    const STATUS_DELIVERED = 'Delivered';
    const STATUS_DECLINED = 'Declined';
    const STATUS_CLOSED = 'Closed';

    const PAY_STATUSES = [
        self::PAY_STATUS_PAID,
        self::PAY_STATUS_NOT,
    ];

    const RUS_PAY_STATUSES = [
        self::PAY_STATUS_NOT => 'Не оплачен',
        self::PAY_STATUS_PAID => 'Оплачен',
    ];

    const PAY_STATUS_PAID = 'Paid';
    const PAY_STATUS_NOT = 'Not paid';

    public function place()
    {
        return $this->belongsTo(Place::class);
    }

    public function telegramUsers()
    {
        return $this->belongsToMany(TelegramUser::class)->withPivot('user_status', 'pay_status', 'id');
    }

    public function otu()
    {
        return $this->hasMany(OrderTelegramUser::class, 'order_id', 'id');
    }

    /**
     * @param $query
     * @param string $status
     * @param TelegramUser $telegramUser
     * @return mixed
     */
    public function scopeStatusAndHasAdmin($query, string $status, TelegramUser $telegramUser)
    {
        return $query->where('status', $status)
            ->hasAdmin($telegramUser);
    }

    /**
     * @param $query
     * @param TelegramUser $telegramUser
     * @return mixed
     */
    public function scopeHasAdmin($query, TelegramUser $telegramUser)
    {
        return $query->whereHas('telegramUsers', function ($q) use ($telegramUser) {
            $q->where('order_telegram_user.user_status', 'Admin')
                ->where('order_telegram_user.telegram_user_id', $telegramUser->id);
        });
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeNotFinished($query)
    {
        return $query->whereNotIn('status', self::FINISHED_STATUSES);
    }

    public function scopeNotClosed($query)
    {
        return $query->where('status', '!=', self::STATUS_CLOSED)->where('status', '!=', self::STATUS_DECLINED);
    }

    public function scopeDoesntHaveTelegramUser($query, TelegramUser $telegramUser)
    {
        return $query->whereDoesntHave('telegramUsers', function ($q) use ($telegramUser) {
            $q->where('telegram_users.id', $telegramUser->id);
        });
    }

    public function scopeHasTelegramUser($query, TelegramUser $telegramUser, array $roles = TelegramUser::USER_ORDER_STATUSES)
    {
        return $query->whereHas('telegramUsers', function ($q) use ($telegramUser, $roles) {
            $q->where('telegram_users.id', $telegramUser->id)->whereIn('order_telegram_user.user_status', $roles);
        });
    }

    public function scopeHasItemsOfUser($query, TelegramUser $telegramUser)
    {
        return $query->whereHas('items', function ($q) use ($telegramUser) {
            $q->where('telegram_user_id', $telegramUser->id);
        });
    }

    public function getAdmins()
    {
        return $this->telegramUsers()->where('order_telegram_user.user_status', TelegramUser::USER_ORDER_STATUS_ADMIN)->get();
    }

    public function getOrderName()
    {
        return "{$this->place->name} - № {$this->id}";
    }


    /**
     * @param TelegramUser $adminUser
     * @param string $text
     * @param bool $requisites
     */
    public function sendMessagesToRecipients(TelegramUser $adminUser, string $text, bool $requisites = false)
    {
        /** @var TelegramUser $recipientUser */
        foreach ($this->telegramUsers as $recipientUser) {
            try {
                $recipientUser->sendMessage($text);
                if (($requisites) && ($recipientUser->pivot->pay_status === Order::PAY_STATUS_NOT)){
                    $textRequisites = $adminUser->requisites ? $adminUser->requisites : "Создатель не указал уточняйте у @{$adminUser->username}";
                    $recipientUser->sendMessage("Реквизиты для оплаты @{$adminUser->username}: {$textRequisites}");
                }else{
                    $recipientUser->sendMessage("Оплата по заказу произведена.");
                }
            } catch (\Exception $exception) {
                $adminUser->sendMessage("Не удалось отправить сообщение пользователю @{$recipientUser->username}. Бот блокирован.");
            }
        }
    }

    public function items()
    {
        return $this->hasMany(Item::class);
    }

    /**
     * @return string
     */
    public function printListOfItems()
    {
        $text = null;
        $count = 0;
        $sum = 0;
        /** @var TelegramUser $telegramUser */
        foreach ($this->telegramUsers as $telegramUser) {
            $countUser = 0;
            $sumUser = 0;
            $text .= "Пользователь: @{$telegramUser->username}\n";
            /** @var Item $item */
            foreach ($telegramUser->getItemsByOrder($this) as $item) {
                $text .= $item->getItemInfo() . PHP_EOL;
                $sumUser += $item->count * $item->price;
                $countUser+= $item->count;
            }
            $count += $countUser;
            $sum += $sumUser;
            $text .= "Итого: {$countUser} шт. Сумма: {$sumUser} руб.\n";
            $payStatusKey = $this->telegramUsers()->where('telegram_users.id', $telegramUser->id)->first()->pivot->pay_status ?? null;
            $text .= self::RUS_PAY_STATUSES[$payStatusKey] . PHP_EOL;
            $text .= PHP_EOL;
        }

        if (!$text) {
            $text = "В заказе {$this->getOrderName()} нет ни одной позиции";
        } else {
            $text .= "Итого по заказу: {$count} шт. Сумма: {$sum} руб.\n";
        }
        return $text;
    }

    public function getUserListOfItems(TelegramUser $telegramUser)
    {
        return $this->items()->where('telegram_user_id', $telegramUser->id)->get();
    }

    public function printListOfUserItems(TelegramUser $telegramUser)
    {
        $text = null;
        /** @var Collection $items */
        $items = $this->getUserListOfItems($telegramUser);
        /** @var Item $item */
        $countUser = 0;
        $sumUser = 0;
        foreach ($items as $item) {
            $text .= "{$item->id}. {$item->getItemInfo()}" . PHP_EOL;
            $sumUser += $item->count * $item->price;
            $countUser++;
        }
        $text .= "Итого: {$countUser} шт. Сумма: {$sumUser} руб.\n";
        return $text;
    }

    public function scopeHasNotPaidUser($query, TelegramUser $telegramUser)
    {
        return $query->whereHas('telegramUsers', function ($q) use ($telegramUser) {
            $q->where('telegram_users.id', $telegramUser->id)->where('order_telegram_user.pay_status', self::PAY_STATUS_NOT);
        });
    }

    public function awareAdmins(TelegramUser $telegramUser, $action = 'что-то сделал с заказом'){
        $adminUsers = $this->getAdmins();
        /** @var TelegramUser $adminUser */
        foreach ($adminUsers as $adminUser) {
            $adminUser->sendMessage("Пользователь @{$telegramUser->username} {$action} {$this->getOrderName()}.");
        }
    }

    public function sendMessageToAdmins($message = 'Какое-то сообщение админу'){
        $adminUsers = $this->getAdmins();
        /** @var TelegramUser $adminUser */
        foreach ($adminUsers as $adminUser) {
            $adminUser->sendMessage("{$message}");
        }
    }

    public function printListOfDefaulterUsers(){
        /** @var Collection $defaulterUsers */
        $defaulterUsers = $this->telegramUsers()->where('pay_status', Order::PAY_STATUS_NOT)->get();
        $text = "Неоплатившие граждане:\n";
        /** @var TelegramUser $defaulterUser */
        foreach ($defaulterUsers as $defaulterUser){
           $text .= "@{$defaulterUser->username}" . PHP_EOL;
       }
       return $text;
    }
    public function telegramUsersItems()
    {
        return $this->belongsToMany(TelegramUser::class, 'items');
    }

}
