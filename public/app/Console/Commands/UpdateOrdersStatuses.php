<?php

namespace App\Console\Commands;

use App\Order;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateOrdersStatuses extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:statuses:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates order statuses';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $statuses = implode(', ', array_map(function ($e) {return "'$e'";},Order::STATUSES));
        $default = ORDER::STATUS_ACTIVE;
        DB::statement("ALTER TABLE orders MODIFY status ENUM({$statuses}) NOT NULL DEFAULT '{$default}'");
        return true;
    }
}
