<?php
/**
 * Created by PhpStorm.
 * User: eso
 * Date: 12.05.19
 * Time: 18:31
 */

namespace App\Services;


use App\Order;
use App\TelegramUser;
use Illuminate\Database\Eloquent\Collection;

class OrderService
{
    /**
     * @param Collection $orders
     * @return string
     */
    public static function getTelegramOrdersList(Collection $orders){
        $text = '';
        /** @var Order $order */
        foreach ($orders as $order) {
            $place = $order->place;
            $text .= sprintf('%s. %s - %s' . PHP_EOL, $order->id, $place->name, $place->url);
            $text .= "Пользователи:\n";
            /** @var TelegramUser $telegramUser */
            foreach ($order->telegramUsers as $telegramUser) {
                if ($telegramUser->pivot->user_status === TelegramUser::USER_ORDER_STATUS_ADMIN){
                    $username = $telegramUser->username ? $telegramUser->username : "CHAT_ID={$telegramUser->id}";
                    $username .= ' - ' . TelegramUser::USER_ORDER_STATUS_ADMIN;
                }else{
                    $username = $telegramUser->username ? $telegramUser->username : "CHAT_ID={$telegramUser->id}";
                }
                $text .= sprintf('%s' . PHP_EOL, "{$username}");
            }
            $text .= PHP_EOL;
        }
        return $text;
    }


}