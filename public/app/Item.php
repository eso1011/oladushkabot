<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer count
 * @property string url
 * @property string name
 * @property float price
 * @property integer id
 */
class Item extends Model
{
    protected $guarded = ['id'];

    public function telegramUser()
    {
        return $this->belongsTo(TelegramUser::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function getItemInfo()
    {
        return "{$this->name}, {$this->count} шт. {$this->price} руб. {$this->url}";
    }
}
