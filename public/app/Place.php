<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string url
 * @property string name
 * @property integer id
 */
class Place extends Model
{
    protected $guarded = [];

    public function orders(){
        return $this->hasMany(Order::class);
    }
}
