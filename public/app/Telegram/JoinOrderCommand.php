<?php

namespace App\Telegram;

use App\Order;
use App\Services\OrderService;
use App\TelegramUser;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Actions;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;

/**
 * Class HelpCommand.
 */
class JoinOrderCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'join';

    /**
     * @var string Command Description
     */
    protected $description = 'Присоединиться к текущему заказу';

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        $this->replyWithChatAction(['action' => Actions::TYPING]);
        $message = Telegram::getWebhookUpdates()['message'];
        /** @var TelegramUser $telegramUser */
        $telegramUser = TelegramUser::find($message['from']['id']);

        $keyboard = [];
        $orders = Order::where('status', Order::STATUS_ACTIVE)->doesntHaveTelegramUser($telegramUser)->get();
        if (count($orders) <= 0) {
            $telegramUser->sendMessage('Нет ни одного заказа, или же вы уже присоединились ко всем существующим. Воспользуйтесь коммандой /orders_list для отображения списка заказов.');
            return null;
        }

        $telegramUser->setCurrentAction([
            'action' => 'join_order_choose_order',
        ]);
        $list = OrderService::getTelegramOrdersList($orders);
        $telegramUser->sendMessage($list);

        /** @var Order $order */
        foreach ($orders as $order) {
            $keyboard[] = [
                ['text' => $order->id],
            ];
        }
        $reply_markup = Keyboard::make([
            'keyboard' => $keyboard,
            'resize_keyboard' => true,
            'one_time_keyboard' => true
        ]);
        $telegramUser->sendMessage('Выберите ID заказа.', $reply_markup);

    }
}
