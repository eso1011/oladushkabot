<?php

namespace App\Telegram;

use App\TelegramUser;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Actions;
use Telegram\Bot\Laravel\Facades\Telegram;

/**
 * Class HelpCommand.
 */
class AddRequisitesCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'update_requisites';

    /**
     * @var string Command Description
     */
    protected $description = 'Добавляет реквизиты для оплаты.';

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        $this->replyWithChatAction(['action' => Actions::TYPING]);
        $message = Telegram::getWebhookUpdates()['message'];
        /** @var TelegramUser $telegramUser */
        $telegramUser = TelegramUser::find($message['from']['id']);

        $telegramUser->setCurrentAction([
            'action' => 'update_requisites',
        ]);
        if ($telegramUser->requisites){
            $telegramUser->sendMessage("Текущие реквизиты: $telegramUser->requisites;");
        }
        $telegramUser->sendMessage("Введите свои реквизиты и примечания, если это необходимо.\nПример: Иванова И.А. 88005553535. Альфа/Сбер. Приоритет альфа.\nПример: Петров П.А. +78005553535. Альфа. Можно наличкой в Австралию на 3 этаже.");
    }
}
