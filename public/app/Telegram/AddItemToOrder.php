<?php

namespace App\Telegram;

use App\Order;
use App\Services\OrderService;
use App\TelegramUser;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Actions;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;

/**
 * Class HelpCommand.
 */
class AddItemToOrder extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'add_item';

    /**
     * @var string Command Description
     */
    protected $description = 'Добавить позицию к заказу';

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        $this->replyWithChatAction(['action' => Actions::TYPING]);
        $message = Telegram::getWebhookUpdates()['message'];
        /** @var TelegramUser $telegramUser */
        $telegramUser = TelegramUser::find($message['from']['id']);

        $keyboard = [];
        $orders = Order::where('status', Order::STATUS_ACTIVE)
            ->hasTelegramUser($telegramUser)
            ->get();
        if (count($orders) <= 0) {
            $telegramUser->sendMessage('Нет ни одного активного заказа. Воспользуйтесь командой /orders_list для отображения списка актинвых заказов.');
            return null;
        }

        $telegramUser->setCurrentAction([
            'action' => 'new_item_select_order',
        ]);
        $list = OrderService::getTelegramOrdersList($orders);
        $telegramUser->sendMessage($list);

        /** @var Order $order */
        foreach ($orders as $order) {
            $keyboard[] = [
                ['text' => $order->id],
            ];
        }
        $reply_markup = Keyboard::make([
            'keyboard' => $keyboard,
            'resize_keyboard' => true,
            'one_time_keyboard' => true
        ]);
        $telegramUser->sendMessage('Выберите ID заказа.', $reply_markup);

    }
}
