<?php

namespace App\Telegram;

use App\Order;
use App\Services\OrderService;
use App\TelegramUser;
use Illuminate\Support\Collection;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;

/**
 * Class HelpCommand.
 */
class GetItemsList extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'items_list';

    /**
     * @var string Command Description
     */
    protected $description = 'Выводит список позиций из заказа.';

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        $this->replyWithChatAction(['action' => Actions::TYPING]);
        $message = Telegram::getWebhookUpdates()['message'];
        /** @var TelegramUser $telegramUser */
        $telegramUser = TelegramUser::find($message['from']['id']);
        $keyboard = [];
        $orders = Order::notClosed()->hasTelegramUser($telegramUser)->get();
        if (count($orders) <= 0) {
            $telegramUser->sendMessage('Нет заказов, в которых бы у вас были бы позиции.');
            return null;
        }

        $telegramUser->setCurrentAction([
            'action' => 'items_list_select_order',
        ]);
        $list = OrderService::getTelegramOrdersList($orders);
        $telegramUser->sendMessage($list);

        /** @var Order $order */
        foreach ($orders as $order) {
            $keyboard[] = [
                ['text' => $order->id],
            ];
        }
        $reply_markup = Keyboard::make([
            'keyboard' => $keyboard,
            'resize_keyboard' => true,
            'one_time_keyboard' => true
        ]);
        $telegramUser->sendMessage('Выберите ID заказа.', $reply_markup);

    }
}
