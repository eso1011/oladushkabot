<?php

namespace App\Telegram;

use App\TelegramUser;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Actions;
use Telegram\Bot\Laravel\Facades\Telegram;

/**
 * Class HelpCommand.
 */
class CreateNewPlaceCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'new_place';

    /**
     * @var string Command Description
     */
    protected $description = 'Создание нового места для доставки.';

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        $this->replyWithChatAction(['action' => Actions::TYPING]);
        $message = Telegram::getWebhookUpdates()['message'];
        /** @var TelegramUser $telegramUser */
        $telegramUser = TelegramUser::find($message['from']['id']);
        if (!$telegramUser){
            $text = "Для выполнения этой команды необходимо зарегистрироваться!";
            $this->replyWithMessage(compact('text'));
            return null;
        }
        $telegramUser->setCurrentAction([
            'action' => 'new_place_set_name',
            'data' => [
                'name' => null,
                'url' => null,
            ],
        ]);
        $telegramUser->sendMessage('Введите название места.');
    }
}
