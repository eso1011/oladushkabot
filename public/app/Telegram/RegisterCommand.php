<?php

namespace App\Telegram;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Actions;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;

/**
 * Class HelpCommand.
 */
class RegisterCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'test';

    /**
     * @var string Command Description
     */
    protected $description = 'Тестова команда';

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        $this->replyWithChatAction(['action' => Actions::TYPING]);

        $data = Telegram::getWebhookUpdates();
        if (isset($data['message'])){
            $message = $data['message'];
        }
        $reply_markup = Keyboard::make()->inline()->row(
            Keyboard::inlineButton(['text'=>'test', 'callback_data'=>'/register'])
        );

        Telegram::sendMessage([
            'chat_id' => $message['from']['id'],
            'text' => 'Hello World',
            'reply_markup' => $reply_markup
        ]);
        $text = sprintf('%s: %s' . PHP_EOL, 'Ваш номер чата', $message['from']['id']);
        $this->replyWithMessage(compact('text'));
    }
}
