<?php

namespace App\Telegram;

use App\Place;
use Telegram\Bot\Commands\Command;

/**
 * Class HelpCommand.
 */
class GetManCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'man';

    /**
     * @var string Command Description
     */
    protected $description = 'Исчерпывающий мануал по работе с ботом.';

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        $botUsername = env('BOT_USERNAME');
        $text =
            "Первым делом перейдите в бота {$botUsername} и активируйте его нажав на кнопку Start." . PHP_EOL .
            PHP_EOL .
            "Инструкция для создателя:" . PHP_EOL .
            "1. Если необходимо обновите реквизиты профиля /update_requisites. Это нужно для автоматической отправки ваших платежных данных пользователям." . PHP_EOL .
            "2. Ознакомьтесь со списком мест /places_list" . PHP_EOL .
            "3. Если необходимо добавьте новое место /new_place" . PHP_EOL .
            "4. Создайте новый заказ /new_order. При присоединении/отсоединении пользователей и добавлении/удалении ими позиций вам будут приходить уведомления." . PHP_EOL .
            "5. Добавьте свои позиции в заказ /add_item" . PHP_EOL .
            "6. Для удаления позиции воспользуйтесь /remove_item" . PHP_EOL .
            "7. Для просмотра всех позиций пользователей и статусов их оплаты воспользуйтесь командой /items_list. Она будет доступна до тех пор, пока все пользователи не оплатят заказ коммандой /pay." . PHP_EOL .
            "8. После подтверждения заказа у оператора воспользуйтесь командой /order_change_status и поставьте статус Confirmed. Пользователи больше не смогут добавляться в заказ и редактировать позиции. Пользователям в заказе придет соответствующее уведомление." . PHP_EOL .
            "9. После того как заказ прибудет в место назначения воспользуйтесь командой /order_change_status и поставьте статус Delivered. Пользователям придет сообщение о доставке и, если они не оплатили, ваши платежные реквизиты." . PHP_EOL .
            "10. Если есть пользователи которые не оплатили заказ, то вам придет список их никнеймов в противном случае заказу проставится статус Closed." . PHP_EOL .
            "11. Если вы хотите отменить заказ воспользуйтесь командой /order_change_status и выберите Declined. Пользователям придет соотвествующее оповещение." . PHP_EOL  .
            PHP_EOL .
            "Инструкция для пользователя:" . PHP_EOL .
            "1. Просмотрите список активных заказов с помощью команды /orders_list" . PHP_EOL .
            "2. Присоеденитесь к заказу /join" . PHP_EOL .
            "3. Добавьте свои позиции в заказ /add_item" . PHP_EOL .
            "4. Для удаления позиции воспользуйтесь /remove_item" . PHP_EOL .
            "5. После того как вам придет оповещение о том, что заказ переведен в статус Подтвержден можно оплатить заказ и уведомить создателя с помощью команды /pay. ВАЖНО!!! Можно оплачивать и после того как заказ был получен, но обязательно не забудьте уведомить об оплаченом заказе, иначе он будет висеть в списке бесконечно." . PHP_EOL .
            "6. Если вы передумали делать заказ покиньте его /leave. Создателю придет соответствующее уведомление." . PHP_EOL .
            "7. Как только создатель сменит статус заказа на Доставлен вам придет соотвествующее уведомление и реквизиты, если вы еще не оплатили заказ." . PHP_EOL .
            PHP_EOL .
            "Общии инструкции: " . PHP_EOL .
            "1. Прервать ввод данных можно с помошью команды /break. Например вы начали создавать новое место /new_place, ввели имя, но затем передумали создавать место. С помощью команды /break вы прервете текущий ввод и вернете бота в исходное состояние." . PHP_EOL .
            PHP_EOL .
            "Остались вопросы? Возникли трудности? Хочешь поддержать проект? Пиши @OldEso."

        ;

        $this->replyWithMessage(compact('text'));
    }
}
