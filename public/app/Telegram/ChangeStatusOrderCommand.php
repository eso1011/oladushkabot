<?php

namespace App\Telegram;

use App\Order;
use App\Services\OrderService;
use App\TelegramUser;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Actions;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;

/**
 * Class HelpCommand.
 */
class ChangeStatusOrderCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'order_change_status';

    /**
     * @var string Command Description
     */
    protected $description = 'Меняет статус заказа.';

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        $this->replyWithChatAction(['action' => Actions::TYPING]);
        $message = Telegram::getWebhookUpdates()['message'];
        /** @var TelegramUser $telegramUser */
        $telegramUser = TelegramUser::find($message['from']['id']);
        $keyboard = [];
        $orders = Order::hasAdmin($telegramUser)->notFinished()->get();

        if (count($orders) <= 0) {
            $telegramUser->sendMessage('У вас нет заказов, в которых вы являетесь админом.');
            return null;
        }
        $list = OrderService::getTelegramOrdersList($orders);
        $telegramUser->sendMessage($list);

        $telegramUser->setCurrentAction([
            'action' => 'change_order_status',
        ]);
        /** @var Order $order */
        foreach ($orders as $order) {
            $keyboard[] = [
                ['text' => $order->id],
            ];
        }
        $reply_markup = Keyboard::make([
            'keyboard' => $keyboard,
            'resize_keyboard' => true,
            'one_time_keyboard' => true
        ]);
        $telegramUser->sendMessage('Выберите ID заказа.', $reply_markup);

    }
}
