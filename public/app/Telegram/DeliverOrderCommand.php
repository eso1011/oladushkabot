<?php

namespace App\Telegram;

use App\Order;
use App\Services\OrderService;
use App\TelegramUser;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Actions;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;

/**
 * Class HelpCommand.
 */
class DeliverOrderCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'order_deliver';

    /**
     * @var string Command Description
     */
    protected $description = 'Переводит заказ из статуса Подтвержден в Доставлен и рассылает уведомления участникам.';

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        $this->replyWithChatAction(['action' => Actions::TYPING]);
        $message = Telegram::getWebhookUpdates()['message'];
        /** @var TelegramUser $telegramUser */
        $telegramUser = TelegramUser::find($message['from']['id']);
        $keyboard = [];
        $orders = Order::statusAndHasAdmin(Order::STATUS_CONFIRMED, $telegramUser)->get();

        if (count($orders) <= 0) {
            $telegramUser->sendMessage('У вас нет заказов со статусом подтвержден, в которых вы являетесь админом.');
            return null;
        }
        $list = OrderService::getTelegramOrdersList($orders);
        $telegramUser->sendMessage($list);

        $telegramUser->setCurrentAction([
            'action' => 'deliver_order_choose_order',
        ]);
        /** @var Order $order */
        foreach ($orders as $order) {
            $keyboard[] = [
                ['text' => $order->id],
            ];
        }
        $reply_markup = Keyboard::make([
            'keyboard' => $keyboard,
            'resize_keyboard' => true,
            'one_time_keyboard' => true
        ]);
        $telegramUser->sendMessage('Выберите ID заказа.', $reply_markup);

    }
}
