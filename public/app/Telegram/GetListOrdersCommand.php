<?php

namespace App\Telegram;

use App\Order;
use App\Services\OrderService;
use Telegram\Bot\Commands\Command;

/**
 * Class HelpCommand.
 */
class GetListOrdersCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'orders_list';

    /**
     * @var string Command Description
     */
    protected $description = 'Выводит список доступных заказов';

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        $text = 'Нет активных заказов.';
        $orders = Order::where(['status' => Order::STATUS_ACTIVE])->get();
        /** @var Order $order */
        if (count($orders) > 0) {
            $text = OrderService::getTelegramOrdersList($orders);
        }
        $this->replyWithMessage(compact('text'));
    }
}
