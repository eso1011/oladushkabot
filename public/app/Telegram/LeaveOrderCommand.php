<?php

namespace App\Telegram;

use App\Order;
use App\Services\OrderService;
use App\TelegramUser;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Actions;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;

/**
 * Class HelpCommand.
 */
class LeaveOrderCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'leave';

    /**
     * @var string Command Description
     */
    protected $description = 'Покинуть активный заказ в котором вы не являетесь админом.';

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        $this->replyWithChatAction(['action' => Actions::TYPING]);
        $message = Telegram::getWebhookUpdates()['message'];
        /** @var TelegramUser $telegramUser */
        $telegramUser = TelegramUser::find($message['from']['id']);

        $keyboard = [];
        $orders = Order::where('status', Order::STATUS_ACTIVE)->hasTelegramUser($telegramUser, [TelegramUser::USER_ORDER_STATUS_USER])->get();
        if (count($orders) <= 0) {
            $telegramUser->sendMessage('Нет ни одного активного заказа, в котором вы являетесь участником. Воспользуйтесь коммандой /orders_list для отображения списка актинвых заказов.');
            return null;
        }

        $telegramUser->setCurrentAction([
            'action' => 'leave_order_choose_order',
        ]);
        $list = OrderService::getTelegramOrdersList($orders);
        $telegramUser->sendMessage($list);

        /** @var Order $order */
        foreach ($orders as $order) {
            $keyboard[] = [
                ['text' => $order->id],
            ];
        }
        $reply_markup = Keyboard::make([
            'keyboard' => $keyboard,
            'resize_keyboard' => true,
            'one_time_keyboard' => true
        ]);
        $telegramUser->sendMessage('Выберите ID заказа.', $reply_markup);

    }
}
