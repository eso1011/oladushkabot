<?php

namespace App\Telegram;
use App\TelegramUser;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Laravel\Facades\Telegram;

/**
 * Class HelpCommand.
 */
class BreakCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'break';

    /**
     * @var string Command Description
     */
    protected $description = 'Прерывает текущие активности и возвращает бота в исходное состояние.';

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        $data = Telegram::getWebhookUpdates();
        $message = $data['message'];
        /** @var TelegramUser $telegramUser */
        $telegramUser = TelegramUser::find($message['from']['id']);
        $telegramUser->sendMessage('Чем я еще могу вам помочь?');
        $this->triggerCommand('help');
    }
}
