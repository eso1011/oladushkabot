<?php

namespace App\Telegram;

use App\Place;
use Telegram\Bot\Commands\Command;

/**
 * Class HelpCommand.
 */
class GetListPlacesCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'places_list';

    /**
     * @var string Command Description
     */
    protected $description = 'Выводит список доступных мест';

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        $text = '';
        $places = Place::all();
        if (count($places) <= 0){
            $text = 'Пока что не создано ни одного места. Воспользуйтесь командой /new_place.';
            $this->replyWithMessage(compact('text'));
            return null;
        }
        foreach ($places as $place){
            $text .= sprintf('%s. %s : %s'.PHP_EOL, $place->id, $place->name, $place->url);
        }
        $this->replyWithMessage(compact('text'));
    }
}
