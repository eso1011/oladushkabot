<?php

namespace App\Telegram;
use App\Place;
use App\TelegramUser;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Actions;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;

/**
 * Class HelpCommand.
 */
class CreateOrderCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'new_order';

    /**
     * @var string Command Description
     */
    protected $description = 'Создание нового заказа.';

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        $this->replyWithChatAction(['action' => Actions::TYPING]);

        $data = Telegram::getWebhookUpdates();
        $message = $data['message'];

        /** @var TelegramUser $telegramUser */
        $telegramUser = TelegramUser::find($message['from']['id']);
        $telegramUser->setCurrentAction([
            'action' => 'new_order_choose_place',
            'data' => [
                'place_id' => null,
            ]
        ]);

        /** @var Place $place */
        $keyboard = [];
        $this->triggerCommand('places_list');
        $places = Place::all();
        if (!$places){
            return null;
        }
        foreach ($places as $place){
            $keyboard[] = [
                ['text'=>$place->id],
            ];
        }
        if ($keyboard){
            $reply_markup = Keyboard::make([
                'keyboard' => $keyboard,
                'resize_keyboard' => true,
                'one_time_keyboard' => true
            ]);
            $telegramUser->sendMessage('Выберите ID доставки.', $reply_markup);
        }

    }
}
